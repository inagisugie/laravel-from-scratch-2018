@extends('projects.layout')

@section('content')
    <h1>Create a new Project</h1>
    <form method="POST" action="/projects">
        @csrf
        <div>
            <input type="text" class="input {{ $errors->has('title') ? 'is-danger':'' }}" name="title"
                   placeholder="Project title" value="{{ old('title') }}"/>
        </div>
        <div>
            <textarea name="description" class="textarea {{ $errors->has('title') ? 'is-danger':'' }}">{{ old('description') }}</textarea>
        </div>
        <button type="submit" class="button is-link">Create Project</button>

        @include('errors')
    </form>
@endsection
