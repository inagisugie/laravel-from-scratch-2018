@extends('projects.layout')

@section('content')
    <h1 class="title">{{ Auth::user()->name ?? 'Not logged in' }}'s Projects</h1>
    <a href="/projects/create" class="btn btn-primary">New</a>
    <ul>
        @foreach( $projects as $project)
            <li>
                <a href="/projects/{{ $project->id }}">
                    {{ $project->title }}
                </a>
            </li>
        @endforeach
    </ul>
@endsection
