<?php

namespace App;


use App\Events\ProjectCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Project extends Model
{
//    protected $fillable = ['title', 'description'];
    protected $guarded = [];

    protected $dispatchesEvents = [
        'created' => ProjectCreated::class,
    ];

    public static function boot()
    {
        parent::boot();
        static::created(function ($project) {
        });
    }

    public function owner()
    {
//        return $this->belongsTo(User::class, 'owner_id');
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function addTask($task)
    {
        $this->tasks()->create($task);
    }
}
