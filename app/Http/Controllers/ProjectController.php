<?php

namespace App\Http\Controllers;

use App\Mail\ProjectCreated;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects.index', ['projects' => $projects = auth()->user()->projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute = $this->validateProject();
        $attribute['owner_id'] = auth()->user()->id;
        $project = Project::create($attribute);
        // event(new \App\Events\ProjectCreated($project)); -> Moved to Project.php $dispatchesEvents
        // php artisan make:mail ProjectCreated --markdown="mail.project-created"
        // http://blog.ice2nd.mobi/index.php/2019/08/16/makemail/
        return redirect('/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        // abort_if
        // abort_unless(auth()->user()->owns($project), 403);
        // $this->authorize('view', $project);
        // if (\Gate::denies('update', $project)) abort(403);
        // if (!\Gate::allows('update', $project)) abort(403);
        // auth()->user()->can('udpate', $project);
        // auth()->user()->cannot('udpate', $project);
        $this->authorize('update', $project);
        return view('projects.show', compact('project'));
    }

    /**
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project)
    {
        $this->authorize('update', $project);
        $attribute = $this->validateProject();
        $project->update($attribute);
        return redirect('/projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect('/projects');
    }

    protected function validateProject()
    {
        return request()->validate([
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:3']
        ]);
    }
}
