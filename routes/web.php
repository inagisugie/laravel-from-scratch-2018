<?php

Route::get('/', function() {
    return view('welcome');
});

Route::get('/notify', function () {
    $user = App\User::get()->last();
    $user->notify( new App\Notifications\SubscriptionRenewalFailed);
    return $user; // Done
});

//Route::resource('/projects', 'ProjectController')->middleware('can:update,project');
Route::resource('/projects', 'ProjectController')->middleware('auth');

Route::post('/projects/{project}/tasks', 'ProjectTasksController@store');
//Route::patch('/tasks/{task}', 'ProjectTasksController@update');

Route::post('/completed-task/{task}', 'CompletedTasksController@store');
Route::delete('/completed-task/{task}', 'CompletedTasksController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
